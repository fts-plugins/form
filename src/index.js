/**
 * BLOCK: @fts/accordion
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

// import './style.scss';
// import './editor.scss';

/**
 * WordPress dependencies
 */
import {
    InnerBlocks,
    InspectorControls,
} from '@wordpress/block-editor';
import {
    PanelBody,
    TextControl,
    CheckboxControl,
    FormTokenField,
} from '@wordpress/components';
import {__} from '@wordpress/i18n';
import {Fragment} from '@wordpress/element';
import {registerBlockType} from '@wordpress/blocks';

/**
 * Internal dependencies
 */
import './fields/phone';
import './fields/text';
import './fields/date';
import './fields/url';

/**
 * Register: Form Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType('fts/form', {
    // Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
    title: __('Form', 'fts-block'), // Block title.
    icon: 'feedback', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
    category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
    keywords: [
        __('fts', 'fts-block'),
        __('form', 'fts-block'),
        __('contact', 'fts-block'),
    ],
    supports: {
        inserter: true,
    },
    attributes: {
        to: {
            type: 'array'
        },
        cc: {
            type: 'array'
        },
        isRedirectedAfterSending: {
            type: 'bool'
        },
        hasDuplicateToSender: {
            type: 'bool'
        },
        errorMessage: {
            type: 'string'
        },
        successMessage: {
            type: 'string'
        },
        urlToRedirect: {
            type: 'string'
        }
    },

    /**
     * The edit function describes the structure of your block in the context of the editor.
     * This represents what the editor will render when the block is used.
     *
     * The "edit" property must be a valid function.
     *
     * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
     */
    edit: ({attributes, setAttributes}) => {

        const ALLOWED_BLOCKS = [
            ['fts/input-text'],
            ['fts/input-phone'],
            ['core/heading'],
            ['core/list']
        ];
        const TEMPLATE = [
            // ['core/button'],
            // ['fts/form']
            // ['fts/input-email']
        ];

        return (
            <Fragment>
                <InspectorControls>
                    {__('Add a form in your document', 'fts-block')}
                    <PanelBody>
                        <FormTokenField
                            label={__('To:', 'fts-block')}
                            value={attributes.to}
                            onChange={(to) => setAttributes({to})}
                        />
                        <FormTokenField
                            label={__('Cc:', 'fts-block')}
                            value={attributes.cc}
                            onChange={(cc) => setAttributes({cc})}
                        />
                        <CheckboxControl
                            label={__('Send a duplicate e-mail to sender', 'fts-block')}
                            checked={attributes.hasDuplicateToSender}
                            onChange={(hasDuplicateToSender) => {
                                setAttributes({hasDuplicateToSender})
                            }}
                        />
                        <TextControl
                            label={__('Error message', 'fts-block')}
                            value={attributes.errorMessage}
                            onChange={(errorMessage) => {
                                setAttributes({errorMessage})
                            }}
                        />
                        <TextControl
                            label={__('Success message', 'fts-block')}
                            value={attributes.successMessage}
                            onChange={(successMessage) => {
                                setAttributes({successMessage})
                            }}
                            disabled={attributes.isRedirectedAfterSending}
                        />
                        <h3>— {__('Or', 'fts-block')} —</h3>
                        <CheckboxControl
                            label={__('Redirect user after sending form', 'fts-block')}
                            checked={attributes.isRedirectedAfterSending}
                            onChange={(isRedirectedAfterSending) => {
                                setAttributes({isRedirectedAfterSending})
                            }}
                        />
                        {(attributes.isRedirectedAfterSending) && (
                            <TextControl
                                label={__('Where to redirect ?', 'fts-block')}
                                value={attributes.urlToRedirect}
                                type="url"
                                onChange={(urlToRedirect) => {
                                    setAttributes({urlToRedirect})
                                }}
                            />
                        )}
                    </PanelBody>
                </InspectorControls>
                <section className="fts-block-form">
                    <div className="wp-block-group__inner-container">
                        <InnerBlocks
                            template={TEMPLATE}
                            templateLock={false}
                            // allowedBlocks={ALLOWED_BLOCKS}
                            renderAppender={() => <InnerBlocks.ButtonBlockAppender/>}
                        />
                    </div>
                </section>
            </Fragment>
        );
    },

    /**
     * The save function defines the way in which the different attributes should be combined
     * into the final markup, which is then serialized by Gutenberg into post_content.
     *
     * The "save" property must be specified and must be a valid function.
     *
     * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
     */
    save: () => {
        return (
            <form className="fts-block-form">
                <InnerBlocks.Content/>
            </form>
        );
    },
});
