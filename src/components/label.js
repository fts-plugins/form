import {__} from '@wordpress/i18n';
import {Component} from '@wordpress/element';
import {RichText} from "@wordpress/block-editor";

export default class Label extends Component {

    render() {
        const {value, onChange} = this.props;
        console.log('onChangePropsFromField: ', onChange);
        console.log('valuePropsFromField: ', value);

        return (
            <RichText
                tagName="small"
                value={value}
                placeholder={
                    /* translators: Label placeholder when has no value */
                    __('Write your label', 'fts-block')
                }
                onChange={(value) => onChange('label', value)}
            />
        )
    }
}
