/**
 * WordPress dependencies
 */
import {__} from '@wordpress/i18n';
import {FormToggle, PanelBody, ToggleControl} from '@wordpress/components';
import {Component, Fragment} from '@wordpress/element';
import {InspectorControls} from "@wordpress/block-editor";

/**
 * Internal dependencies
 */
import Label from "./label";
import InputHelp from "./input-help";

/**
 * Generic field
 */
export default class Field extends Component {

    render() {
        const {type, onChange, description, hasDescription, required, label} = this.props;

        return (
            <Fragment>
                <InspectorControls>
                    <PanelBody>
                        <ToggleControl
                            label={__('Required', 'fts-block')}
                            help={required ? __('The field is needed.', 'fts-block') : __('The field is not needed.', 'fts-block')}
                            checked={required}
                            onChange={(required) => {
                                onChange('required', required)
                            }}
                        />
                    </PanelBody>
                </InspectorControls>

                <FormToggle
                    checked={required}
                    onChange={(e) => {
                        onChange('required', e.target.checked)
                    }}
                />

                <Label
                    value={label}
                    onChange={onChange}
                />

                <input type={type} style={{
                    width: '100%',
                    minHeight: '36px'
                }}/>

                <InputHelp hasDescription={hasDescription}
                           description={description}
                           onChange={onChange}
                />
            </Fragment>
        )
    }
}
