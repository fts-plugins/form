import {__} from '@wordpress/i18n';
import {withState} from '@wordpress/compose';
import {Component, Fragment} from '@wordpress/element';
import {PanelBody, ToggleControl} from '@wordpress/components';
import {InspectorControls, RichText} from "@wordpress/block-editor";


export default class InputHelp extends Component {

    render() {
        const {description, hasDescription, onChange} = this.props;

        return (
            <Fragment>
                <InspectorControls>
                    <ToggleControl
                        label={__('Enable/disable description', 'fts-block')}
                        help={hasDescription ? __('Hide description', 'fts-block') : __('Show description', 'fts-block')}
                        checked={hasDescription}
                        onChange={() => {
                            onChange('hasDescription', !hasDescription)
                        }}
                    />
                </InspectorControls>
                {
                    (hasDescription) && (
                        <RichText
                            tagName="small"
                            value={description}
                            placeholder={
                                /* translators: Label placeholder when has no value */
                                __('Description example', 'fts-block')
                            }
                            onChange={(description) => {
                                onChange('description', description)
                            }}
                        />
                    )
                }
            </Fragment>
        )
    }
}
