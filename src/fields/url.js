/**
 * BLOCK: @fts/accordion
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

// import './style.scss';

/**
 * WordPress dependencies
 */
import {__} from '@wordpress/i18n';
import {registerBlockType} from '@wordpress/blocks';
import {G, Path, SVG} from '@wordpress/components';

/**
 * Internal dependencies
 */
import Field from "../components/field";

const svgIcon = () => (
    <SVG xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 24 24" height="24" width="24">
        <G transform="matrix(1,0,0,1,0,0)">
            <Path d="M 6.75,17.249l10.5-10.5 " stroke="#000000" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            <Path d="M 7.735,12.021c-1.272-0.073-2.515,0.4-3.417,1.3l-2.25,2.25 c-1.757,1.757-1.757,4.607,0,6.364s4.607,1.757,6.364,0l0,0l2.25-2.25c0.9-0.902,1.373-2.145,1.3-3.417 " stroke="#000000" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            <Path d="M 16.265,11.976 c1.272,0.073,2.515-0.4,3.417-1.3l2.25-2.25c1.757-1.757,1.757-4.607,0-6.364s-4.607-1.757-6.364,0l-2.25,2.25 c-0.898,0.903-1.369,2.146-1.295,3.417" stroke="#000000" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
        </G>
    </SVG>
);

/**
 * Register: Phone field Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType('fts/input-date', {
    // Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
    title: __('Date field', 'fts-block'), // Block title.
    icon: svgIcon, // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
    category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
    keywords: [
        __('fts', 'fts-block'),
        __('form', 'fts-block'),
        __('date', 'fts-block'),
        __('input', 'fts-block'),
    ],
    supports: {
        inserter: true,
    },
    parent: ['fts/form'],
    attributes: {
        label: {
            type: 'children',
            default: ''
        },
        description: {
            type: 'children',
            default: ''
        },
        hasDescription: {
            type: 'bool',
            default: true
        },
        required: {
            type: 'bool',
            default: false
        },
    },

    /**
     * The edit function describes the structure of your block in the context of the editor.
     * This represents what the editor will render when the block is used.
     *
     * The "edit" property must be a valid function.
     *
     * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
     */
    edit: ({attributes, setAttributes}) => {

        const onChange = (key, value) => {
            setAttributes({[key]: value})
        };

        return (
            <Field type="url" onChange={onChange} {...attributes} />
        );
    },

    /**
     * The save function defines the way in which the different attributes should be combined
     * into the final markup, which is then serialized by Gutenberg into post_content.
     *
     * The "save" property must be specified and must be a valid function.
     *
     * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
     */
    save: () => {
        return;
    },
});
