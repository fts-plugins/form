/**
 * BLOCK: @fts/accordion
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

// import './style.scss';

/**
 * WordPress dependencies
 */
import {__} from '@wordpress/i18n';
import {registerBlockType} from '@wordpress/blocks';
import {G, Path, SVG} from '@wordpress/components';

/**
 * Internal dependencies
 */
import Field from "../components/field";

const svgIcon = () => (
    <SVG xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 24 24" height="24" width="24">
        <G transform="matrix(1,0,0,1,0,0)">
            <Path d="M 23.25,15.748c0,0.828-0.672,1.5-1.5,1.5c0,0,0,0,0,0 " stroke="#000000" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            <Path d="M 21.75,6.748c0.828,0,1.5,0.672,1.5,1.5 " stroke="#000000" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            <Path d="M 0.75,8.248 c0-0.828,0.672-1.5,1.5-1.5 " stroke="#000000" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            <Path d="M 2.25,17.248c-0.828,0-1.5-0.672-1.5-1.5 " stroke="#000000" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            <Path d="M 0.75,11.248v1.5 " stroke="#000000" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            <Path d="M 23.25,11.248v1.5 " stroke="#000000" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            <Path d="M 5.25,6.748h1.5 " stroke="#000000" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            <Path d="M 9.75,6.748h1.5 " stroke="#000000" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            <Path d="M 17.25,6.748h1.5 " stroke="#000000" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            <Path d="M 5.25,17.248h1.5 " stroke="#000000" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            <Path d="M 9.75,17.248h1.5 " stroke="#000000" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            <Path d="M 17.25,17.248h1.5 " stroke="#000000" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            <Path d="M 14.25,18.748v-13.5 " stroke="#000000" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            <Path d="M 9.75,23.248 c2.485,0,4.5-2.015,4.5-4.5c0,2.485,2.015,4.5,4.5,4.5 " stroke="#000000" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            <Path d="M 9.75,0.748c2.485,0,4.5,2.015,4.5,4.5l0,0c0-2.485,2.015-4.5,4.5-4.5 " stroke="#000000" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            <Path d="M 11.25,14.248h6" stroke="#000000" fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
        </G>
    </SVG>);

/**
 * Register: Phone field Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType('fts/input-text', {
    // Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
    title: __('Text field', 'fts-block'), // Block title.
    icon: svgIcon,
        // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
    category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
    keywords: [
        __('fts', 'fts-block'),
        __('form', 'fts-block'),
        __('text', 'fts-block'),
        __('input', 'fts-block'),
    ],
    supports: {
        inserter: true,
    },
    parent: ['fts/form'],
    attributes: {
        label: {
            type: 'children'
        },
        description: {
            type: 'children',
        },
        hasDescription: {
            type: 'bool'
        },
        required: {
            type: 'bool'
        },
    },

    /**
     * The edit function describes the structure of your block in the context of the editor.
     * This represents what the editor will render when the block is used.
     *
     * The "edit" property must be a valid function.
     *
     * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
     */
    edit: ({attributes, setAttributes}) => {

        const onChange = (key, value) => {
            setAttributes({[key]: value})
        };

        return (
            <Field type="text" onChange={onChange} {...attributes} />
        );
    },

    /**
     * The save function defines the way in which the different attributes should be combined
     * into the final markup, which is then serialized by Gutenberg into post_content.
     *
     * The "save" property must be specified and must be a valid function.
     *
     * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
     */
    save: () => {
        return;
    },
});
