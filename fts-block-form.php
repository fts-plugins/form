<?php
/**
 * Plugin Name: @fts-block/form
 * Plugin URI: https://github.com/ahmadawais/create-guten-block/
 * Description: @fts-block/form is a Gutenberg block to create forms in content.
 * Author: Fantassin
 * Author URI: https://www.fantassin.fr/
 * Version: 1.0.0
 * License: GPL2+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue Gutenberg block assets for both frontend + backend.
 *
 * @uses {wp-editor} for WP editor styles.
 * @since 1.0.0
 */
function fts_block_form_assets() {
	// Styles.
	wp_enqueue_style(
		'fts-block-form-style', // Handle.
		plugins_url( 'dist/blocks.style.build.css', dirname( __FILE__ ) ), // Block style CSS.
		array( 'wp-editor' ) // Dependency to include the CSS after it.
	// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.style.build.css' ) // Version: File modification time.
	);
}

// Hook: Frontend assets.
//add_action( 'enqueue_block_assets', 'fts_block_form_assets' );

/**
 * Enqueue Gutenberg block assets for backend editor.
 *
 * @uses {wp-blocks} for block type registration & related functions.
 * @uses {wp-element} for WP Element abstraction — structure of blocks.
 * @uses {wp-i18n} to internationalize the block's text.
 * @uses {wp-editor} for WP editor styles.
 * @since 1.0.0
 */
function fts_block_form_editor_assets() {
	// Scripts.
	wp_enqueue_script(
		'fts-block-form-script', // Handle.
		plugins_url( '/build/index.js', __FILE__ ), // Block.build.js: We register the block here. Built with Webpack.
		[],
		array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor' ), // Dependencies, defined above.
		// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: File modification time.
		true // Enqueue the script in the footer.
	);

	// Styles.
//	wp_enqueue_style(
//		'accordion-block-editor-style', // Handle.
//		plugins_url( 'dist/blocks.editor.build.css', dirname( __FILE__ ) ), // Block editor CSS.
//		array( 'wp-edit-blocks', 'accordion-block-style' ) // Dependency to include the CSS after it.
//	// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.editor.build.css' ) // Version: File modification time.
//	);
}

// Hook: Editor assets.
add_action( 'enqueue_block_editor_assets', 'fts_block_form_editor_assets' );
